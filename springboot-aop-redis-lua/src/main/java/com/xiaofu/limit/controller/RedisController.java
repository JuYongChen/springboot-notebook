package com.xiaofu.limit.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.TreeMap;

/**
 * @Author: xiaofu
 * @Description:
 */
@Controller
public class RedisController  {

    @Resource
    private RedisTemplate<String, Serializable> redisTemplate;

    @GetMapping("/addKey")
    @ResponseBody
    public String addkey() {

        for (int i = 0; i < 50; i++) {

            redisTemplate.opsForValue().set("test_key_" + i, i);
        }
        return null;
    }

    @GetMapping("/setRedisList")
    @ResponseBody
    public String setRedisList() {

        redisTemplate.opsForList().leftPush("222",1);
        redisTemplate.opsForList().leftPush("222",1);
        redisTemplate.opsForList().leftPush("222",1);
        redisTemplate.opsForList().leftPush("222",1);
        redisTemplate.opsForList().leftPush("222",1);
        redisTemplate.opsForList().leftPush("222",1);

        System.out.println(redisTemplate.opsForList().size("222"));

        return null;
    }

    @GetMapping("/setRedisStr")
    @ResponseBody
    public String setRedisStr() {

        for (int i = 0; i < 20; i++) {
            redisTemplate.opsForValue().increment("333",1);
        }


        return null;
    }


    @GetMapping("/getRedisList")
    @ResponseBody
    public String getRedisList() {

        for (int i = 0; i < 5000; i++) {

            new Thread(()->{
               try {
                   Integer skuIdExist = (Integer) redisTemplate.opsForList().leftPop("222");
//                   System.out.println(Thread.currentThread().getName() + " ： 过来抢");
                   if(skuIdExist != null){
                    System.out.println(Thread.currentThread().getName() + " ： 拿到了");
                   }
                }catch (Exception e){

               }

            }, "thread" + i).start();
        }

        return "ok";
    }

    @GetMapping("/getRedisStr")
    @ResponseBody
    public String getRedisStr() {

        for (int i = 0; i < 5000; i++) {
            new Thread(()->{
                try {
                    Long increment = redisTemplate.opsForValue().increment("333", -1);
                    if(increment > 0 ){
                        System.out.println(Thread.currentThread().getName() + " ： 拿到了: " +increment);
                    }
                }catch (Exception e){

                }

            }, "thread" + i).start();
        }

        return "ok";
    }
}